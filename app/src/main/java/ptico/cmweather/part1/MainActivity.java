package ptico.cmweather.part1;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import ptico.cmweather.R;
import ptico.cmweather.WeatherParser;

public class MainActivity extends AppCompatActivity {


    private ArrayAdapter<String> forecastListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // FIXME: Temporary, for testing. Needs to be refactored.
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

/*
        String[] data = {
                "Mon 6/23 - Sunny - 31/17",
                "Tue 6/24 - Foggy - 21/8",
                "Wed 6/25 - Cloudy - 22/17",
                "Thurs 6/26 - Rainy - 18/11",
                "Fri 6/27 - Foggy - 21/10",
                "Sat 6/28 - TRAPPED IN WEATHERSTATION - 23/18",
                "Sun 6/29 - Sunny - 20/7"
        };
        List<String> daysLabels = new ArrayList<String>(Arrays.asList(data));

        forecastListAdapter = new ArrayAdapter<String>(
                this, // The current context (this activity)
                R.layout.list_item_meteo, // The name of the layout ID.
                R.id.textDayForecast, // The ID of the textview to populate.
                daysLabels);
*/
        forecastListAdapter = new ArrayAdapter<String>(
                this, // The current context (this activity)
                R.layout.daily_list_item_layout, // The name of the layout ID.
                R.id.textDayForecast, // The ID of the textview to populate.
                new ArrayList<String>() ); // a collection of string entries

        // Get a reference to the ListView, and attach this adapter to it.
        ListView listView = (ListView) findViewById(R.id.listView_week_forecast);
        listView.setAdapter(forecastListAdapter);


        // call live data and refresh the list
        String jsonResults = WeatherParser.callOpenWeatherForecast();
        updateWeatherInfo( jsonResults);

        // listen to selections in the list
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Toast.makeText(getApplicationContext(),
                        "Selected: " + parent.getAdapter().getItem( position ), Toast.LENGTH_LONG)
                        .show();
                startDetailsActivity( parent.getAdapter().getItem( position ).toString() );

            }
        });

    }

    private void startDetailsActivity(String selection) {
        Intent intent = new Intent(this, DetailsActivity.class);
        intent.putExtra(DetailsActivity.EXTRA_ARG_SELECTED_OPTION, selection);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_view_map:
                showMap();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    // calls the remote API and puts results in the list
    private void updateWeatherInfo( String jsonResults ) {
        try {
            String[] entries = WeatherParser.getWeatherDataFromJson(jsonResults, 7);
            if (entries != null) {
                forecastListAdapter.clear();
                for (String dayForecastStr : entries) {
                    forecastListAdapter.add(dayForecastStr);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }



    private void showMap() {
        // Build the intent
        Uri location = Uri.parse("geo:0,0?q=Aveiro,+Portugal");
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, location);

        // Verify it resolves
        PackageManager packageManager = getPackageManager();
        List<ResolveInfo> activities = packageManager.queryIntentActivities(mapIntent, 0);

        // Start an activity if it's safe
        if (activities.size() > 0) {
            startActivity(mapIntent);
        } else {
            Toast.makeText( this, "No app available to show the map", Toast.LENGTH_SHORT).show();
        }
    }
}
