package ptico.cmweather.part1;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import ptico.cmweather.R;
import ptico.cmweather.part2.DetailsFragment;

public class DetailsActivity extends AppCompatActivity  {

    static final String EXTRA_ARG_SELECTED_OPTION = "ptico.arg_selection";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        // has any args been passed in the intent?
        Intent intent = getIntent();
        String message = intent.getStringExtra(EXTRA_ARG_SELECTED_OPTION);

        TextView textDetails = (TextView)findViewById(R.id.tvDetails);
        textDetails.setText(message);

    }

    private void showDetailsFragment(String selection) {
        // Create fragment and give it an argument specifying the selection it should show
        DetailsFragment detailsFragment = new DetailsFragment();
        Bundle args = new Bundle();
        args.putString(DetailsFragment.ARG_SELECTED_DAY, selection);
        detailsFragment.setArguments(args);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack so the user can navigate back
        transaction.replace(R.id.fragment_placeholder, detailsFragment );
        transaction.addToBackStack(null);

        // Commit the transaction
        transaction.commit();

    }


}
