package ptico.cmweather.part2;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import ptico.cmweather.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class DetailsFragment extends Fragment {


    public static final String ARG_SELECTED_DAY = "ptico.arg_selected_day";

    public DetailsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_details, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        // During startup, check if there are arguments passed to the fragment.
        // onStart is a good place to do this because the layout has already been
        // applied to the fragment
        Bundle args = getArguments();
        if (args != null) {
            // Set article based on argument passed in
            updateDetailsView(args.getString(ARG_SELECTED_DAY));
        }
    }

    /**
     * called when the hosting activity ask this fragment to update info
     */
    public void updateDetailsView(String dayWeather) {

        TextView textView = (TextView) getActivity().findViewById(R.id.tv_frag_details);
        textView.setText(dayWeather);

    }
}
