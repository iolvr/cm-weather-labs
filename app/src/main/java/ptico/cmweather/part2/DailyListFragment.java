package ptico.cmweather.part2;


import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.json.JSONException;

import java.util.ArrayList;

import ptico.cmweather.R;
import ptico.cmweather.WeatherParser;


/**
 * holds the list of days with forecasts
 */
public class DailyListFragment extends Fragment {


    private ArrayAdapter<String> weatherForecastListAdapter;
    private OnDaySelectedListener myHostingActivity;


    // The container Activity must implement this interface so the frag can deliver events
    public interface OnDaySelectedListener {

        /** to be called by DailyListFragment when a list item is selected */
        void onDaySelected(String dayWeather);
    }


    public DailyListFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment ans save the root view
        View rootView = inflater.inflate(R.layout.fragment_daily_list, container, false);

        // prepare the list adapter
        weatherForecastListAdapter = new ArrayAdapter<String>(
                getActivity(), // The current context (this activity)
                R.layout.daily_list_item_layout, // The name of the layout ID.
                R.id.textDayForecast, // The ID of the textview to populate.
                new ArrayList<String>() ); // a collection of string entries

        // Get a reference to the ListView, and attach this adapter to it.
        ListView listView = (ListView) rootView.findViewById(R.id.list_of_days);
        listView.setAdapter(weatherForecastListAdapter);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                // Notify the parent activity of selected item
                myHostingActivity.onDaySelected(adapterView.getAdapter().getItem(position).toString());

                // Set the item as checked to be highlighted when in two-pane layout
                ((ListView) adapterView).setItemChecked(position, true);

            }
        });

        setHasOptionsMenu(true);

        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.daily_view_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_refresh_forecast:
                refresh_weather_data();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * call live data and refresh the list
     * uses the AsyncTask for a background thread
     */
    private void refresh_weather_data() {
        FetchWeatherFromServerTask weatherTask = new FetchWeatherFromServerTask();
        weatherTask.execute();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception.
        try {
            myHostingActivity = (OnDaySelectedListener) getActivity();
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnDaySelectedListener");
        }
    }

    /**
     * at this moment, the fragment already has the layout elements; lets force the update of the list
     */
    @Override
    public void onStart() {
        super.onStart();
        refresh_weather_data();
    }


    public class FetchWeatherFromServerTask extends AsyncTask<String, Void, String[]> {

        private final String LOG_TAG = FetchWeatherFromServerTask.class.getSimpleName();


        @Override
        protected String[] doInBackground(String... params) {
            // call live data and refresh the list
            String[] entries = null;
            String jsonResults = WeatherParser.callOpenWeatherForecast();
            try {
                 entries = WeatherParser.getWeatherDataFromJson(jsonResults, 7);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return entries;
        }

        @Override
        protected void onPostExecute(String[] entries) {
            if (entries != null) {
                    weatherForecastListAdapter.clear();
                    for (String dayForecastStr : entries) {
                        weatherForecastListAdapter.add(dayForecastStr);
                    }
                }
            }

    }

}


